package com.example.demo.facturas;

import com.example.demo.productos.ProductosServices;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class FacturasController {
    @Autowired FacturasRepository facturasRepository;

    @GetMapping("/facturas")
    public Iterable<FacturaModel> getFacturas(@RequestParam(defaultValue = "0") double hasta) {
        if (hasta == 0) {
            return facturasRepository.findAll();
        } else {
            return facturasRepository.findByImporte(hasta);
        }

    }
    @GetMapping("/facturas/fecha")
    public Iterable<FacturaModel> getFacturasFecha(@RequestParam(defaultValue = "") String fecha) {
        if (fecha == "") {
            return facturasRepository.findAll();
        } else {
            return facturasRepository.findByFecha(fecha);
        }

    }

    @Autowired
    ProductosServices productosServices;
    @PostMapping("/facturas")
    public String crearFactura(@RequestParam String cliente, @RequestParam int idProducto){
        try {
            productosServices.crearFactura(cliente, idProducto);
            return "OK";
        }catch (Exception ex){
            return  "KO";
        }
    }


}
