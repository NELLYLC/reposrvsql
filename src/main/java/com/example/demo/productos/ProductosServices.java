package com.example.demo.productos;

import com.example.demo.facturas.FacturaModel;
import com.example.demo.facturas.FacturasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class ProductosServices {

    @Autowired
    ProductosRepository productosRepository;
    @Autowired
    FacturasRepository facturasRepository;

    @Transactional(rollbackFor = {Exception.class})
    public void  crearFactura(String cliente, int idProducto) throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
        String fecha = sdf.format(new Date());
        Optional<ProductoModel> optProd = productosRepository.findById(idProducto);
        if (optProd.isPresent()){
            ProductoModel prod = optProd.get();
            FacturaModel fn = new FacturaModel();
            fn.setCliente(cliente);
            fn.setIdProducto(idProducto);
            fn.setFecha(fecha);
            fn.setImporte(prod.getPrecio());
            facturasRepository.save(fn);

            if ((prod.getStock() -1) < 0) throw new Exception("stock < 0");
            prod.setStock(prod.getStock() -1);
            productosRepository.save(prod);

        }
    }
}
