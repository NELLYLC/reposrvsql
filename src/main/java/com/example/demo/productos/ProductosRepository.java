package com.example.demo.productos;

import org.springframework.data.repository.CrudRepository;

public interface ProductosRepository extends CrudRepository<ProductoModel, Integer> {
}
